FROM node:16.15.1

WORKDIR /home/src/app

ENV LANG fr_FR.UTF-8

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 8000


CMD ["npm", "run", "dev" ]
