# Rest API

# Sujet: Développer une application gérant des Pass donnant accès à des User à des zones protégées.


## Stack Techniques

Le kata peut être réalisé avec une de ces 2 stacks:

* Javascript: Node.JS + Express + MongoDB _(utiliser le .nvmrc et package.json fournis)_
* Python: Flask + MongoDB


## Implémentation


### Logique métier
![schema](./media-assets/entity-diagram.png)

Votre application se base tout d'abord sur la modélisation d'entités: `User`, `Pass` et `Place`

Les `User` ont des `Pass`. Ces `Pass` leur donnent l'accès à des `Place` moyennant un niveau d'accès, _level_, un entier compris entre 1 et 5.

Les `Place` mettent également une contrainte sur l'_age_ du `User`.

Un `User` a accès à une `Place` si:
* il dispose d'un `Pass` avec un niveau d'accès suffisant
* son _age_ est suffisant


### API

Cette logique métier doit être servie par une API Rest. Voici les routes attendues:


- 🛠 [CRUD](https://developer.mozilla.org/fr/docs/Glossary/CRUD) operations pour toutes les entités

- 🚦 Une route pour vérifier si un `User` a accès à une `Place`

- 🚦 Une route pour obtenir la liste des `Place` accessibles par un `User`


### Persistance

Implémentez la persistance des entités avec la BDD specifiée dans votre Stack Technique.

Créer un fichier .json à la base du repository, chargé dans la BDD au lancement du serveur.

### Containerisation

Pour permettre au reviewer de corriger votre kata, il faut nécessairement fournir un projet **containerisé**:
* Dockerfile générant une image pour l'API.
* docker-compose.yml pour lancer l'API + la database Mongo

___

## ⚠️ Candidatures et modalités de rendu ⚠️

> A ce stade, vous avez implémenté une API Rest assez basique servant une logique métier. Ce kata donne une grande importance à **l'industrialisation** que vous développez autour. Voici une liste d'améliorations possibles du projet. Implémentez celles qui vous semblent les plus pertinantes (à indiquer dans un readme !)

> **Pour le rendu, Poussez sur une nouvelle branche git, ouvrez une merge request vers Main, et notifiez votre interlocuteur par message que le kata est fini.**

### Améliorations

* Sécurité
    * Ajouter un endpoint pour générer un token d'accès
    * Protéger les routes derrière le token d'accès
    * Un `Pass` ne doit pouvoir être accédé que par son `User`

* Tests
    * Couverture de test Jest
    * Lancement de la test-suite via un script npm ou bash

* Documentation / Interface
    * Specification des routes en format Swagger
    * Ajout d'un front swagger pour faciliter le testing manuel des routes

* Fullstack:
    * Création d'un frontend


# Motivation & Contexte

Mettre rapidement en place une API sans passer par de framework haut-niveau, et en respectant une stack technique donnée. Mettre en place des best-practice d'industrialisation pour faciliter la mise à disposition de l'application, assurer sa sécurité & la qualité du code rendu.



# Specification [RFC2119](https://microformats.org/wiki/rfc-2119-fr) du kata

> Description précise & sans ambiguité sur les termes de ce qui est attendu


* Le candidat `PEUT` proposer & justifier une autre piste d'amélioration.  Ces améliorations `DOIVENT` être documentées dans le Readme.
* Le candidat `DOIT` implémenter les codes de retour suivant: 200, 201, 400, 401, 403, 404, 500.
* L'API `DOIT` être accessible via curl.
* Le candidat `DEVRAIT` structurer son code dans une logique Models/Controllers/Routes
* Le candidat `DOIT` respecter la stack technique qui lui est demandée.
* La commande _"docker-compose up"_  `DOIT` permettre d'accéder au serveur localement.


# Launching the app
## Prerequisites

Before you begin, ensure you have met the following requirements:
<!--- These are just example requirements. Add, duplicate or remove as required --->
* If you have a `Windows`, `Linux` or a `Mac` machine with `docker`installed, that's fine. You're good to go. 👌
* If you have `make`install, that should probably be the case if you're on Mac or linux, it's even better. Things gonna be more simple. 
* Rename the `.env.example` file to `.env`. Then copy and then modify the content of the `.env`if want to use your own `MongoDB ATLAS` cluster and a different `JWT` secret value.

You don't need to do it because I let my own `.env` file in the project even though I'm aware of the fact that it's not a best pratice. But, I do it to facilate you the setup.

## Run and manage the app with docker without `make`
### Run the app
```sh
docker compose up --build # For the first time you run the app
```

```sh
docker compose up # the other times you run the app if youu dow-n't down the app
```

### Down the app

```sh
docker compose down
```

## Run and manage the app with `make`
**NOTE**: You have several commands inside the `Makefile`in the root folder of the app to handle in a more simple way the management of the app

```sh
make up-build # Equivalent to the `up --build`'s of the docker command one
```

```sh
make up # Equivalent to the `up`'s docker command one
```

**IMPORTANT**: If you want to rebuild the container quickly, you can simply do the following command:

```sh
make rebuild
```

## Tech stack
  * Node.js and its ecosystem
  * MongoDB ATLAS for storage
  * Docker & docker-compose for containerization

## Tools
  * Postman to test the API endpoints (**Note**: A Postman collection called `ExaltIT.postman_collection.json` is embedded in the root of the project in order to allow to test directly the API with Postman in your side.)
## Implemented features

* Users' endpoints
  * Get all users
  * Get a single user by its id
  * Create a new user
  * Update an existing user
  * Delete an existing user
  * Get all user's allowed places
  * Check if a user has access to a given place
  
* Passes' endpoints
  * Get all passes
  * Get a single pass by its id
  * Create a new pass
  * Update an existing pass
  * Delete an existing pass
  
* Places' endpoints
  * Get all places
  * Get a single place by its id
  * Create a new place
  * Update an existing place
  * Delete an existing place

* Auth endpoints
  * Registration
  * Login (with a JWT token generated)
  * Logout
  * Security
    * Protect all the routes with a middleware that check if the user is auuthenticated when needed
    * Only allowed a user to get a pass if it posesses the pass
  
* Swagger Integration
  * Documents Auth endpoints
  * Documents User endpoints
  * Documents Place endpoints
  * Documents Pass endpoints
## What Can be done next
  * Finish the Swagger Integration
  * All the other mentioned bonus features


## Troubleshouting
If you got the error below with the `make` command
![schema](./media-assets/makefile-error-with-vscode.png)

You just need to do the fowing thing:

** Click in the bottow nav bar of your vscode editor, in the tab titled **Spaces** like the below image.

![schema](./media-assets/spaces-tab-vscode.png)

An then do the instructions of the second image:

![schema](./media-assets/troubleshouting-make-eror-in-vscode.png)
