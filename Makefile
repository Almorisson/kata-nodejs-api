up:
	docker-compose up -d
	docker logs -f exalt_it_kata_api
up-build:
	docker-compose up --build -d
	docker logs -f exalt_it_kata_api
down:
	docker-compose down

rebuild: down up-build
