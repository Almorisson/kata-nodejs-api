import mongoose from 'mongoose'

const PlaceSchema = mongoose.Schema({
    // We  control the unique identifier name to be more explicit
    place_id: {
        type: String,
        required: true,
        trim: true,
        unique: true,
        default: () => { return new mongoose.Types.ObjectId().toString() }
    },
    phone_number: {
        type: String,
        required: true,
        trim: true,
        unique: true,
    },
    address: {
        "street": {
            type: String,
            required: false,
            trim: true,
            default: ""
        },
        "city": {
            type: String,
            required: false,
            trim: true,
            default: ""
        },
        "zip_code": {
            type: Number,
            required: false,
            trim: true,
            default: undefined
        }
    },
    required_pass_level: {
        type: Number,
        required: true,
        trim: true,
        min: 1,
        max: 5,
        default: 1
    },
    required_age_level: {
        type: Number,
        required: true,
        trim: true,
        min: 1,
        default: 16
    },
}, {
    timestamps: true, // add createdAt and updatedAt fields
    versionKey: false, // disable versioning (__v field)
    id: false, // disable _id field
})


PlaceSchema.set('toJSON', { virtuals: true }) // include the virtual fields when using toJSON

export default mongoose.model("Place", PlaceSchema)
