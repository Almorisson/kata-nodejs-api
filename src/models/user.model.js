import mongoose from 'mongoose'

const UserSchema = mongoose.Schema({
    // We  control the unique identifier name to be more explicit
    user_id: {
        type: String,
        required: true,
        trim: true,
        unique: true,
        default: () => { return new mongoose.Types.ObjectId().toString() }
    },
    email: {
        type: String,
        trim: true,
        required: true,
        unique: true,
        min: 6,
        max: 32
    },
    password: {
        type: String,
        required: true,
        min: 6,
        max: 64,
        select: false // hide the password field by default when querying a user document
    },
    first_name: {
        type: String,
        required: true,
        trim: true,
    },
    last_name: {
        type: String,
        required: true,
        trim: true,
    },
    age: {
        type: Number,
        required: true,
        trim: true,
        min: 1,
        default: 16
    },
    phone_number: {
        type: String,
        required: false,
        trim: true
    },
    address: {
        "street": {
            type: String,
            required: false,
            trim: true,
            default: ""
        },
        "city": {
            type: String,
            required: false,
            trim: true,
            default: ""
        },
        "zip_code": {
            type: Number,
            required: false,
            trim: true,
            default: undefined
        }
    },
    passes: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Pass',
        // Map the reference field to the 'pass_id' property of the Pass document
        // instead of the '_id' property
        foreignField: 'pass_id',
        // Populate the passes field with the passes of the user
        autopopulate: { maxDepth: 2 }
    }]
}, {
    timestamps: true, // add createdAt and updatedAt fields
    versionKey: false, // disable versioning (__v field)
    id: false // disable _id field
});

UserSchema.set('toJSON', { virtuals: true }); // include the virtual field when using toJSON

export default mongoose.model("User", UserSchema)
