import mongoose from 'mongoose'

const PassSchema = mongoose.Schema({
    // We  control the unique identifier name to be more explicit
    pass_id: {
        type: String,
        required: true,
        trim: true,
        unique: true,
        default: () => { return new mongoose.Types.ObjectId().toString() }
    },
    level: {
        type: Number,
        required: true,
        trim: true,
        min: 1,
        max: 5,
    }
    // We use the timestamp to know when the pass was created and when it's updated
}, {
    timestamps: true, // add createdAt and updatedAt fields
    versionKey: false, // disable versioning (__v field)
    id: false // disable _id field
})

PassSchema.set('toJSON', { virtuals: true }) // include the virtual fields when using toJSON

export default mongoose.model("Pass", PassSchema)
