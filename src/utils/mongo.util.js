import mongoose from 'mongoose'

export const connect = async () => {
    try {
        await mongoose.connect(process.env.MONGODB_URL,
            {
                useNewUrlParser: true,
                useUnifiedTopology: true
            }
        );
        console.log("Connected to MongoDB ATLAS successfully");
    }
    catch (err) {
        console.log(err);
        throw new Error(err);
    }
}

export const disconnect = async () => {
    try {
        await mongoose.disconnect();
    }
    catch (err) {
        console.log(err)
        throw new Error(err);
    }
}
