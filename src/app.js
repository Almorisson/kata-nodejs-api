import dotenv from 'dotenv'
import express from 'express'
import cors from 'cors'
import swaggerJsdoc from 'swagger-jsdoc'
import swaggerUi from 'swagger-ui-express'

import usersRouter from './routes/users/user.route'
import passesRouter from './routes/passes/pass.route'
import placesRouter from './routes/places/place.route'
import userPassesRouter from './routes/users/user-passes.route'
import userPlacesRouter from './routes/users/user-places.route'
import authRouter from './routes/auth/auth.route'

import swaggerBasicInfo from './docs/info.doc'
import swaggerServers from './docs/servers.doc'
import swaggerTags from './docs/tags.doc'
import swaggerComponents from './docs/components.doc'
import swaggerPaths from './docs/paths/index'
import validationHandler from './validators/validation-handler.validator'

// Load dotenv for .env files usage
dotenv.config();


const app = express();

app.use(cors());
app.use(express.json())

const options = {
    definition: {
        openapi: "3.0.3",
        ...swaggerBasicInfo,
        ...swaggerServers,
        ...swaggerTags,
        ...swaggerComponents,
        ...swaggerPaths,
    },
    schemes: ["http", "https"],
    consumes: ["application/json"],
    produces: ["application/json"],
    // Path to the API docs
    apis: ["./routes/**/*.route.js"], // Update the path according to your routes
};

const specs = swaggerJsdoc(options);

// Run the validationHandler middleware on every request to the api in order to validate the request
app.use((req, res, next) => {
    try {
        validationHandler(req);
        next(); // Pass the request to the next middleware and avoid blocking the execution workflow
    } catch (err) {
        console.log(err);
        next(err);
    }
})

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(specs));
app.use("/auth", authRouter);
app.use("/users", usersRouter);
app.use("/users", userPassesRouter);
app.use("/users", userPlacesRouter);
app.use("/passes", passesRouter);
app.use("/places", placesRouter);

// Test route
app.get('/', (_, res) => {
    res.send('Welcome to ExaltIT API')
})

export default app
