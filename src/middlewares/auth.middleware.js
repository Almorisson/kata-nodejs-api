import jwt from 'jsonwebtoken'

export const authMiddleware = (req, res, next) => {
    const authHeader = req.headers['authorization'];

    const token = authHeader && authHeader.split(' ')[1];

    if (!token) {
        if (req.path === '/logout') {
            return res.status(200).send({ message: 'You are already logged out !' });
        }
        return res.status(401).send({ error: 'Missing token !' });
    }

    jwt.verify(token, process.env.JWT_SECRET_KEY, (err, user) => {
        if (err) return res.status(401).send({ error: err.message });
        req.user = user;
        next();
    });
};
