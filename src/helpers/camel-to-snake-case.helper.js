import _ from 'lodash'

export const camelToSnakeCase = (obj) => {
    // If null or undefined return empty object
    if (obj === null || typeof obj !== 'object') {
        return obj;
    }
    // If the object contains an object, recursively apply camelToSnakeCase in order to convert the nested objects
    const result = {};
    for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
            const snakeCaseKey = _.snakeCase(key);
            result[snakeCaseKey] = camelToSnakeCase(obj[key]);
        }
    }
    return result;
}
