import app from './app'
import { connect } from './utils/mongo.util'

const PORT = process.env.PORT || 8000;

connect(); // connect to MongoDB

app.listen(PORT, () => {
    console.log(`Server is listening on port: ${PORT}`);
})
