export default {
    info: {
        title: "Kata Node.js Exalt IT API Documentation",
        version: "1.0.0",
        description: "API documentation using Swagger",
        contact: {
            name: "Mor NDIAYE",
            email: "ndiayemor164@gmail.com",
            url: "https://github.com/Almorisson",
        },
    },
};
