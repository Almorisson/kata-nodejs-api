const userId = {
    user_id: {
        type: "string",
        description: "User identification number",
        example: "6461e38eb243a2bc659c34c8",
    },
}

const passId = {
    pass_id: {
        type: "string",
        description: "Pass identification number",
        example: "6461e38eb243a2bc659c34c8",
    },
}

const placeId = {
    place_id: {
        type: "string",
        description: "Place identification number",
        example: "6461e38eb243a2bc659c34c8",
    },
}

const addressInput = {
    address: {
        type: "object",
        properties: {
            street: {
                type: "string",
                description: "User street",
                example: "20 Rue des Devs JS",
            },
            city: {
                type: "string",
                description: "User city",
                example: "Paris",
            },
            zipCode: {
                type: "number",
                description: "User zip code",
                example: 78015,
            },
        },
    }
}
const addressOutput = {
    street: {
        type: "string",
        description: "User street",
        example: "20 Rue des Devs JS",
    },
    city: {
        type: "string",
        description: "User city",
        example: "Paris",
    },
    zip_code: {
        type: "number",
        description: "User zip code",
        example: 78015,
    },
}

const userInputFragment = {
    firstName: {
        type: "string",
        description: "User first name",
        example: "Mountakha Talla",
    },
    lastName: {
        type: "string",
        description: "User last name",
        example: "NDIAYE",
    },
    age: {
        type: "number",
        description: "User age",
        example: 25,
    },
    phoneNumber: {
        type: "string",
        description: "User phone number",
        example: "0661654533",
    },
    ...addressInput,
    email: {
        type: "string",
        description: "User email",
        example: "test1245@gmail.com",
    },
    password: {
        type: "string",
        description: "User password",
        example: "test1234",
    },
}

const userOutputFragment = {
    first_name: {
        type: "string",
        description: "User first name",
        example: "Mountakha Talla",
    },
    last_name: {
        type: "string",
        description: "User last name",
        example: "NDIAYE",
    },
    age: {
        type: "number",
        description: "User age",
        example: 25,
    },
    phone_number: {
        type: "string",
        description: "User phone number",
        example: "0661654533",
    },
    ...addressOutput,
    email: {
        type: "string",
        description: "User email",
        example: "test1245@gmail.com",
    },
    password: {
        type: "string",
        description: "User password",
        example: "test1234",
    },
}


export default {
    components: {
        securitySchemes: {
            // Bearer Authorization
            BearerAuth: {
                type: "http",
                scheme: "bearer",
                bearerFormat: "JWT",
            },
        },
        schemas: {
            // Register input model
            RegisterInput: {
                type: "object",
                properties: {
                    ...userInputFragment,
                },
            },

            // Register output model
            RegisterOutput: {
                type: "object",
                properties: {
                    ...userInputFragment,
                    message: {
                        type: "string",
                        description: "User is registered successfully!",
                    },
                },
            },

            // Login input model
            LoginInput: {
                type: "object",
                properties: {
                    email: {
                        type: "string",
                        description: "User email",
                        example: "test6@gmail.com",
                    },
                    password: {
                        type: "string",
                        description: "User password",
                        example: "test123",
                    },
                },
            },

            // Logout output model
            LogoutOutput: {
                type: "object",
                properties: {
                    message: {
                        type: "string",
                        description: "User is User logged out successfully!",
                    },
                },
            },
            // id model
            ...userId,

            User: {
                type: "object",
                properties: {
                    ...userId,
                    ...userOutputFragment,
                },
            },
            // User input model
            UserInput: {
                type: "object",
                properties: {
                    ...userInputFragment
                },
            },

            // pass id model
            ...passId,
            // Pass model
            Pass: {
                type: "object",
                properties: {
                    ...passId,
                    level: {
                        type: "number",
                        description: "Pass level",
                        example: 2,
                    },
                },
            },
            // Pass input model
            PassInput: {
                type: "object",
                properties: {
                    level: {
                        type: "number",
                        description: "Pass level",
                        example: 2,
                    },
                },
            },

            // place id model
            ...placeId,
            // Place model
            Place: {
                type: "object",
                properties: {
                    ...placeId,
                    phone_number: {
                        type: "string",
                        description: "Place phone number",
                        example: "0761483345",
                    },
                    ...addressOutput,
                    required_pass_level: {
                        type: "number",
                        description: "Place required pass level",
                        example: 2,
                    },
                    required_age_level: {
                        type: "number",
                        description: "Place required age level",
                        example: 29,
                    },

                },
            },

            // Place input model
            PlaceInput: {
                type: "object",
                properties: {
                    phoneNumber: {
                        type: "string",
                        description: "Place phone number",
                        example: "0761483345",
                    },
                    ...addressInput,
                    requiredPassLevel: {
                        type: "number",
                        description: "Place required pass level",
                        example: 2,
                    },
                    requiredAgeLevel: {
                        type: "number",
                        description: "Place required age level",
                        example: 29,
                    },
                },
            },
            // Error model
            Error: {
                type: "object",
                properties: {
                    message: {
                        type: "string",
                        description: "Error message",
                        example: "Not found",
                    },
                    internal_code: {
                        type: "string",
                        description: "Error internal code",
                        example: "Invalid parameters",
                    },
                },
            },
        },
    }
}
