export default {
    post: {
        tags: ["Authentication"],
        description: "Login user",
        operationId: "loginUser",
        parameters: [],
        requestBody: {
            content: {
                "application/json": {
                    schema: {
                        $ref: "#/components/schemas/LoginInput",
                    },
                },
            },
        },
        responses: {
            200: {
                description: "User logged in successfully",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/User",
                        },
                    },
                },
            },
            400: {
                description: "Invalid request payload",
            },
            404: {
                description: "User is not found",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/Error",
                        },
                    },
                },
            },
        },
    },
};
