import loginPath from "./login.path"
import logoutPath from "./logout.path"
import registerPath from "./register.path"

export default {
    "/auth/register": {
        ...registerPath,
    },
    "/auth/login": {
        ...loginPath,
    },
    "/auth/logout": {
        ...logoutPath,
    },
}
