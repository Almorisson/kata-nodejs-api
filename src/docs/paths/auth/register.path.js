export default {
    post: {
        tags: ["Authentication"],
        description: "Register user",
        operationId: "registerUser",
        parameters: [],
        requestBody: {
            content: {
                "application/json": {
                    schema: {
                        $ref: "#/components/schemas/RegisterInput",
                    },
                },
            },
        },
        responses: {
            200: {
                description: "User registered successfully",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/RegisterOutput",
                        },
                    },
                },
            },
            400: {
                description: "Invalid request payload",
            },
            404: {
                description: "User is not found",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/Error",
                        },
                    },
                },
            },
        },
    },
};
