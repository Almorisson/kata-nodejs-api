export default {
    get: {
        tags: ["Authentication"],
        description: "Logout user",
        operationId: "logoutUser",
        parameters: [],
        security: [
            {
                BearerAuth: [], // we use the BearerAuth schema defined in components.doc.js
            },
        ],
        responses: {
            200: {
                description: "User logged out successfully",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/LogoutOutput",
                        },
                    },
                },
            },
            400: {
                description: "Invalid request payload",
            },
            404: {
                description: "User is not found",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/Error",
                        },
                    },
                },
            },
        },
    },
};
