export default {
    put: {
        tags: ["User CRUD operations"],
        description: "Update a user",
        operationId: "updateUser", // unique operation id.
        parameters: [
            {
                name: "user_id", // name of the param
                in: "path", // location of the param
                schema: {
                    $ref: "#/components/schemas/user_id", // data model of the param
                },
                required: true, // Mandatory param
                description: "Update a single user by its id",
            },
        ],
        security: [
            {
                BearerAuth: [], // we use the BearerAuth schema defined in components.doc.js
            },
        ],
        requestBody: {
            content: {
                "application/json": {
                    schema: {
                        $ref: "#/components/schemas/UserInput", // user input data model
                    },
                },
            },
        },
        responses: {
            200: {
                description: "User updated successfully",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/User",
                        },
                    },
                },
            },
            400: {
                description: "Invalid request payload",
            },
            404: {
                description: "User is not found",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/Error",
                        },
                    },
                },
            },
        },
    },
};

