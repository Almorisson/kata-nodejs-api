import getUsersPath from './get-users.path'
import createUserPath from './create-user.path'
import getUserByIdPath from './get-user-by-id.path'
import updateUserPath from './update-user.path'
import deleteUserPath from './delete-user.path'
import getUserAllowedPlaces from './get-user-allowed-places.path'
import checkUserHasAccessPlace from './check-user-access-place.path'
import suppressAPassToUser from './suppress-pass-to-user.path'
import assignAPassToAUser from './assign-pass-to-user.path'

export default {
    '/users': {
        ...getUsersPath,
        ...createUserPath
    },
    '/users/{user_id}': {
        ...getUserByIdPath,
        ...updateUserPath,
        ...deleteUserPath
    },
    '/users/{user_id}/places': {
        ...getUserAllowedPlaces,
    },
    '/users/{user_id}/passes/{pass_id}': {
        ...suppressAPassToUser,
        ...assignAPassToAUser,
    },
    '/users/{user_id}/places/{place_id}': {
        ...checkUserHasAccessPlace,
    },
}
