export default {
    get: {
        tags: ["User CRUD operations"],
        description: "Get all users",
        operationId: "getUsers", // unique operation id.
        parameters: [],
        security: [
            {
                BearerAuth: [], // we use the BearerAuth schema defined in components.doc.js
            },
        ],
        responses: {
            200: {
                description: "Retrieved users' list",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/User",
                        },
                    },
                },
            },
        },
    },
};
