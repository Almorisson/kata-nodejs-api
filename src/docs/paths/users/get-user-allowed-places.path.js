export default {
    get: {
        tags: ["User CRUD operations"],
        description: "Get all user's allowed places",
        operationId: "userAllowedPlaces", // unique operation id.
        parameters: [
            {
                name: "user_id", // name of the param
                in: "path", // location of the param
                schema: {
                    $ref: "#/components/schemas/user_id", // data model of the param
                },
                required: true, // Mandatory param
                description: "Get all user's allowed places",
            },
        ],
        security: [
            {
                BearerAuth: [], // we use the BearerAuth schema defined in components.doc.js
            },
        ],
        responses: {
            200: {
                description: "We got the user's allowed places",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/Place", // Pass data model
                        },
                    },
                },
            },
            404: {
                description: "User is not found",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/Error", // error data model
                        },
                    },
                },
            },
        },
    },
};
