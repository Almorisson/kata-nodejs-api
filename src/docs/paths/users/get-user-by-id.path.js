export default {
    get: {
        tags: ["User CRUD operations"],
        description: "Get a User",
        operationId: "getUser", // unique operation id
        parameters: [
            {
                name: "user_id", // name of the param
                in: "path", // location of the param
                schema: {
                    $ref: "#/components/schemas/user_id", // data model of the param
                },
                required: true, // Mandatory param
                description: "Get a single User by its id",
            },
        ],
        security: [
            {
                BearerAuth: [], // we use the BearerAuth schema defined in components.doc.js
            },
        ],
        responses: {
            200: {
                description: "We got the user",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/User", // User data model
                        },
                    },
                },
            },
            404: {
                description: "User is not found",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/Error", // error data model
                        },
                    },
                },
            },
        },
    },
};
