export default {
    delete: {
        tags: ["User CRUD operations"],
        description: "Delete a user",
        operationId: "deleteUser", // unique operation id.
        parameters: [
            {
                name: "user_id", // name of the param
                in: "path", // location of the param
                schema: {
                    $ref: "#/components/schemas/user_id", // data model of the param
                },
                required: true, // Mandatory param
                description: "Delete a single user by its id",
            },
        ],
        security: [
            {
                BearerAuth: [], // we use the BearerAuth schema defined in components.doc.js
            },
        ],
        responses: {
            200: {
                description: "User deleted successfully",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/User",
                        },
                    },
                },
            },
            404: {
                description: "User is not found",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/Error",
                        },
                    },
                },
            },
        },
    },
};
