export default {
    get: {
        tags: ["User CRUD operations"],
        description: "Check if the user has access to a given place",
        operationId: "isUserHasAccessToPlace", // unique operation id.
        parameters: [
            {
                name: "user_id", // name of the param
                in: "path", // location of the param
                schema: {
                    $ref: "#/components/schemas/user_id", // data model of the param
                },
                required: true, // Mandatory param
                description: "The user unique identifier",
            },
            {
                name: "place_id", // name of the param
                in: "path", // location of the param
                schema: {
                    $ref: "#/components/schemas/place_id", // data model of the param
                },
                required: true, // Mandatory param
                description: "The place unique identifier",
            },
        ],
        security: [
            {
                BearerAuth: [], // we use the BearerAuth schema defined in components.doc.js
            },
        ],
        responses: {
            200: {
                description: "We got the user's allowed places",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/Place", // Pass data model
                        },
                    },
                },
            },
            404: {
                description: "User is not found",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/Error", // error data model
                        },
                    },
                },
            },
        },
    },
};
