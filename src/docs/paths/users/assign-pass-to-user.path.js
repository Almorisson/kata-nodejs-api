export default {
    put: {
        tags: ["User CRUD operations"], // operation's tag.
        description: "Assign a pass to a user",
        operationId: "assignPassToUser", // unique operation id.
        parameters: [
            {
                name: "user_id", // name of the param
                in: "path", // location of the param
                schema: {
                    $ref: "#/components/schemas/user_id", // data model of the param
                },
                required: true, // Mandatory param
                description: "Assign a pass to a user",
            },
            {
                name: "pass_id", // name of the param
                in: "path", // location of the param
                schema: {
                    $ref: "#/components/schemas/pass_id", // data model of the param
                },
            }
        ],
        security: [
            {
                BearerAuth: [], // we use the BearerAuth schema defined in components.doc.js
            },
        ],
        responses: {
            200: {
                description: "Pass assigned successfully",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/Pass", // Pass data model
                        },
                    },
                },
            },
            404: {
                description: "User is not found",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/Error", // error data model
                        },
                    },
                },
            },
        },
    },
};
