export default {
    post: {
        tags: ["User CRUD operations"], // operation's tag.
        description: "Create a new user",
        operationId: "createUser", // unique operation id.
        parameters: [],
        security: [
            {
                BearerAuth: [], // we use the BearerAuth schema defined in components.doc.js
            },
        ],
        requestBody: {
            content: {
                "application/json": {
                    schema: {
                        $ref: "#/components/schemas/UserInput", // user input data model
                    },
                },
            },
        },
        responses: {
            201: {
                description: "User created successfully",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/User",
                        },
                    },
                },
            },
            400: {
                description: "Invalid request payload",
            },
        },
    },
};
