export default {
    put: {
        tags: ["Place CRUD operations"],
        description: "Update place",
        operationId: "updatePlace",
        parameters: [
            {
                name: "place_id", // name of the param
                in: "path", // location of the param
                schema: {
                    $ref: "#/components/schemas/placeId", // data model of the param
                },
                required: true, // Mandatory param
                description: "Update a single place by its id",
            },
        ],
        requestBody: {
            content: {
                "application/json": {
                    schema: {
                        $ref: "#/components/schemas/PlaceInput", // place input data model
                    },
                },
            },
        },
        responses: {
            200: {
                description: "Place updated successfully",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/Place",
                        },
                    },
                },
            },
            400: {
                description: "Invalid request payload",
            },
            404: {
                description: "Place is not found",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/Error",
                        },
                    },
                },
            },
        },
    },
};
