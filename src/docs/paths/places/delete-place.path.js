export default {
    delete: {
        tags: ["Place CRUD operations"],
        description: "Delete place",
        operationId: "deletePlace", // unique operation id.
        parameters: [
            {
                name: "place_id", // name of the param
                in: "path", // location of the param
                schema: {
                    $ref: "#/components/schemas/placeId", // data model of the param
                },
                required: true, // Mandatory param
                description: "Delete a single place by its id",
            },
        ],
        security: [
            {
                BearerAuth: [], // we use the BearerAuth schema defined in components.doc.js
            },
        ],
        responses: {
            200: {
                description: "Place deleted successfully",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/Place",
                        },
                    },
                },
            },
            404: {
                description: "Place is not found",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/Error",
                        },
                    },
                },
            },
        },
    },
};
