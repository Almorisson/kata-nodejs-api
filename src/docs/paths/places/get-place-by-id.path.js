export default {
    get: {
        tags: ["Place CRUD operations"],
        description: "Get a place by its id",
        operationId: "getPlaceById", // unique operation id.
        parameters: [
            {
                name: "place_id", // name of the param
                in: "path", // location of the param
                schema: {
                    $ref: "#/components/schemas/placeId", // data model of the param
                },
                required: true, // Mandatory param
                description: "Get a single place by its id",
            },
        ],
        security: [
            {
                BearerAuth: [], // we use the BearerAuth schema defined in components.doc.js
            },
        ],
        responses: {
            200: {
                description: "We got the place",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/Place", // Pass data model
                        },
                    },
                },
            },
            404: {
                description: "Pass is not found",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/Error", // error data model
                        },
                    },
                },
            },
        },
    },
};
