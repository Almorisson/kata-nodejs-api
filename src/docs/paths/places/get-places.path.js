export default {
    get: {
        tags: ["Place CRUD operations"],
        description: "Get all passes",
        operationId: "getPlaces", // unique operation id.
        parameters: [],
        security: [
            {
                BearerAuth: [], // we use the BearerAuth schema defined in components.doc.js
            },
        ],
        responses: {
            200: {
                description: "Retrieved passes' list",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/Place",
                        },
                    },
                },
            },
        },
    },
};
