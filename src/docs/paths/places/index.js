import getPlacesPath from "./get-places.path"
import createPlacePath from "./create-place.path"
import getPlaceByIdPath from "./get-place-by-id.path"
import updatePlacePath from "./update-place.path"
import deletePlacePath from "./delete-place.path"

export default {
    "/places": {
        ...getPlacesPath,
        ...createPlacePath
    },
    "/places/{place_id}": {
        ...getPlaceByIdPath,
        ...updatePlacePath,
        ...deletePlacePath
    }
}
