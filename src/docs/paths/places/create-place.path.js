export default {
    post: {
        tags: ["Place CRUD operations"],
        description: "Create new place",
        operationId: "createPlace",
        parameters: [],
        security: [
            {
                BearerAuth: [], // we use the BearerAuth schema defined in components.doc.js
            },
        ],
        requestBody: {
            content: {
                "application/json": {
                    schema: {
                        $ref: "#/components/schemas/PlaceInput",
                    },
                },
            },
        },
        responses: {
            201: {
                description: "Place created successfully",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/Place",
                        },
                    },
                },
            },
            400: {
                description: "Invalid request payload",
            },
        },
    },
};
