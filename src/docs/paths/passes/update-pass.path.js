export default {
    put: {
        tags: ["Pass CRUD operations"],
        description: "Update a pass",
        operationId: "updatePass", // unique operation id.
        parameters: [
            {
                name: "pass_id", // name of the param
                in: "path", // location of the param
                schema: {
                    $ref: "#/components/schemas/passId", // data model of the param
                },
                required: true, // Mandatory param
                description: "Update a single pass by its id",
            },
        ],
        security: [
            {
                BearerAuth: [], // we use the BearerAuth schema defined in components.doc.js
            },
        ],
        requestBody: {
            content: {
                "application/json": {
                    schema: {
                        $ref: "#/components/schemas/PassInput", // pass input data model
                    },
                },
            },
        },
        responses: {
            200: {
                description: "Pass updated successfully",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/Pass",
                        },
                    },
                },
            },
            400: {
                description: "Invalid request payload",
            },
            404: {
                description: "Pass is not found",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/Error",
                        },
                    },
                },
            },
        },
    },
};
