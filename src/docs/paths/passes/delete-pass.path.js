export default {
    delete: {
        tags: ["Pass CRUD operations"],
        description: "Delete a pass by its id",
        operationId: "deletePass", // unique operation id.
        parameters: [
            {
                name: "pass_id", // name of the param
                in: "path", // location of the param
                schema: {
                    $ref: "#/components/schemas/passId", // data model of the param
                },
                required: true, // Mandatory param
                description: "Delete a single pass by its id",
            },
        ],
        security: [
            {
                BearerAuth: [], // we use the BearerAuth schema defined in components.doc.js
            },
        ],
        responses: {
            200: {
                description: "Pass deleted successfully",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/Pass",
                        },
                    },
                },
            },
            404: {
                description: "Pass is not found",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/Error",
                        },
                    },
                },
            },
        },
    },
};
