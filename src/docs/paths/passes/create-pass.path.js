export default {
    post: {
        tags: ["Pass CRUD operations"], // operation's tag.
        description: "Create a new pass",
        operationId: "createPass", // unique operation id.
        parameters: [],
        security: [
            {
                BearerAuth: [], // we use the BearerAuth schema defined in components.doc.js
            },
        ],
        requestBody: {
            content: {
                "application/json": {
                    schema: {
                        $ref: "#/components/schemas/PassInput", // pass input data model
                    },
                },
            },
        },
        responses: {
            201: {
                description: "Pass created successfully",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/Pass",
                        },
                    },
                },
            },
            400: {
                description: "Invalid request payload",
            },
        },
    },
};
