export default {
    get: {
        tags: ["Pass CRUD operations"],
        description: "Get a pass by id",
        operationId: "getPassById", // unique operation id
        parameters: [
            {
                name: "pass_id", // name of the param
                in: "path", // location of the param
                schema: {
                    $ref: "#/components/schemas/passId", // data model of the param
                },
                required: true, // Mandatory param
                description: "Get a single pass by its id",
            },
        ],
        security: [
            {
                BearerAuth: [], // we use the BearerAuth schema defined in components.doc.js
            },
        ],
        responses: {
            200: {
                description: "We got the pass",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/Pass", // Pass data model
                        },
                    },
                },
            },
            404: {
                description: "Pass is not found",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/Error", // error data model
                        },
                    },
                },
            },
        },
    },
};
