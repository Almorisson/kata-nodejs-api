import getPassesPath from './get-passes.path'
import createPassPath from './create-pass.path'
import getPassByIdPath from './get-pass-by-id.path'
import updatePassPath from './update-pass.path'
import deletePassPath from './delete-pass.path'

export default {
    '/passes': {
        ...getPassesPath,
        ...createPassPath
    },
    '/pass_id}': {
        ...getPassByIdPath,
        ...updatePassPath,
        ...deletePassPath
    }
}
