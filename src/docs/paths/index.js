import usersPaths from './users/index'
import passesPaths from './passes/index'
import placesPaths from './places/index'
import authPath from './auth/index'


export default {
    paths: {
        ...usersPaths,
        ...passesPaths,
        ...placesPaths,
        ...authPath,
    }
}
