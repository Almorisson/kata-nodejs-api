export default {
    tags: [
        {
            name: "Authentication",
        },
        {
            name: "User CRUD operations",
        },
        {
            name: "Place CRUD operations",
        },
        {
            name: "Pass CRUD operations",
        },
    ],
};
