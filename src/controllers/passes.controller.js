import Pass from '../models/pass.model'
import User from '../models/user.model'


export const getAllPasses = async (_, res, next) => {
    try {
        const Passes = await Pass.find({});
        if (Passes && Passes.length > 0) {
            res.status(200).json(Passes);
        }
        else {
            res.status(404).json({ message: "No Pass found!" });
        }
    }
    catch (err) {
        next(err);
    }
}

export const getPassById = async (req, res, next) => {
    try {
        const passId = req.params.pass_id;
        const userId = req.user.user_id // I get the user_id from the user object in the request
        const user = await User.findOne({ user_id: userId }).populate({ path: 'passes', model: 'Pass' });

        if (!user) {
            return res.status(404).json({ message: 'User not found' });
        }

        // Get all user's passes
        const userPassesIds = user.passes.map(pass => pass.pass_id);

        const pass = await Pass.findOne({ pass_id: passId })

        if (!pass) {
            return res.status(404).json({ message: 'Pass not found' });
        }

        // Check if the pass belongs to the user
        if (!userPassesIds.includes(passId)) {
            return res.status(403).json({ message: 'Unauthorized' });
        }

        res.json({ pass });
    }
    catch (err) {
        next(err);
    }
}

export const createPass = async (req, res, next) => {
    try {
        const { level } = req.body;
        const pass = new Pass({
            level
        });

        // save the pass
        pass.save().then(pass => {
            res.status(201).json(pass);
        }).catch(err => {
            res.status(400).json(err);
            next(err);
        });
    } catch (err) {
        next(err)
    }
}

export const updatePass = async (req, res, next) => {
    try {
        const existingPass = await Pass.findOne({ pass_id: req.params.pass_id });
        if (existingPass) {
            const { level } = req.body;
            existingPass.level = level;
            await existingPass.save();
            res.status(200).json(existingPass);
        } else {
            res.status(404).json({ message: "The Pass you are trying to update is not found!" });
        }
    }
    catch (err) {
        res.status(500).json(err);
        next(err)
    }
}

export const deletePass = async (req, res, next) => {
    try {
        await Pass.findOneAndDelete({ pass_id: req.params.pass_id }).then(pass => {
            if (pass) {
                // res.status(204).json(pass); I prefer to return a clear message. That's why I use 200 instead of 204
                res.status(200).json({ message: "Pass deleted successfully!" });
            } else {
                res.status(404).json({ message: "Pass not found!" });
            }
        }).catch(err => {
            res.status(500).json(err);
            next(err);
        })
    } catch (err) {
        next(err)
    }

}
