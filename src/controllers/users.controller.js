import User from '../models/user.model';
import Place from '../models/place.model';
import Pass from '../models/pass.model';
import { camelToSnakeCase } from '../helpers/camel-to-snake-case.helper'

export const getAllUsers = async (_, res, next) => {
    try {
        const users = await User.find({}).populate({ path: 'passes', model: 'Pass' });
        if (users && users.length > 0) {
            res.status(200).json(users);
        }
        else {
            res.status(404).json({ message: "No user found!" });
        }
    }
    catch (err) {
        next(err);
    }
}

export const getUserById = async (req, res, next) => {
    try {
        const user = await User.findOne({ user_id: req.params.user_id }).populate({ path: 'passes', model: 'Pass' });

        if (user) {
            res.status(200).json(user);
        }
        else {
            res.status(404).send({ message: "User not found!" });
        }
    }
    catch (err) {
        next(err);
    }
}

export const createUser = async (req, res, next) => {
    try {
        const user = new User({
            ...camelToSnakeCase(req.body)
        });

        // save the user
        user.save().then(data => {
            res.status(201).json(data);
        }).catch(err => {
            res.status(500).json(err);
            next(err);
        });
    } catch (err) {
        next(err)
    }
}

export const updateUser = async (req, res, next) => {
    try {
        const user = await User.findOne({ user_id: req.params.user_id });
        if (!user) {
            res.status(404).json({ message: "User not found!" });
            return;
        }
        const updatedUser = {
            ...user.toObject(),
            ...camelToSnakeCase(req.body),
            address: {
                ...user.address.toObject(),
                ...camelToSnakeCase(req.body.address)
            }
        };

        await User.findOneAndUpdate(
            { user_id: req.params.user_id },
            { $set: updatedUser },
            {
                useFindAndModify: false,
                new: true
            }
        ).then(user => {
            if (user) {
                res.status(200).json(user);
            }
            else {
                res.status(404).json({ message: "User not found!" });
            }
        }).catch(err => {
            res.status(500).json(err);
            next(err);
        })
    }
    catch (err) {
        res.status(500).json(err);
        next(err)
    }
}

export const deleteUser = async (req, res, next) => {
    try {
        await User.findOneAndDelete({ user_id: req.params.user_id }).then(user => {
            if (user) {
                // res.status(204).json(user); I prefer to return a clear message. That's why I use 200 instead of 204
                res.status(200).json({ message: "User deleted successfully!" });
            }
            else {
                res.status(404).json({ message: "User not found!" });
            }
        }).catch(err => {
            res.status(500).json(err);
            next(err);
        })
    } catch (err) {
        next(err)
    }

}

export const assignAPassToAUser = async (req, res, next) => {
    try {
        // Find the user
        const user = await User.findOne({ user_id: req.params.user_id });
        if (!user) {
            res.status(404).json({ message: "User not found!" });
        }
        // Find the pass
        const pass = await Pass.findOne({ pass_id: req.params.pass_id });
        if (!pass) {
            res.status(404).json({ message: "Pass not found!" });
        }

        // Check if the user already has the pass assigned
        if (user.passes.includes(pass._id)) {
            return res.status(409).json({ message: "The pass is already assigned to the user !" });
        }

        // Assign the pass to the user by updating the user's passes list
        user?.passes.push(pass);

        // Save the user
        user.save().then(user => {
            res.status(201).json(user);
        }
        ).catch(err => {
            res.status(400).json(err);
            next(err);
        });

    } catch (err) {
        next(err)
    }
}

export const suppressAPassToAUser = async (req, res, next) => {
    try {
        // Find the user
        const user = await User.findOne({ user_id: req.params.user_id });
        if (!user) {
            res.status(404).json({ message: "User not found!" });
        }
        // Find the pass
        const pass = await Pass.findOne({ pass_id: req.params.pass_id });
        if (!pass) {
            res.status(404).json({ message: "Pass not found!" });
        }
        // Check if the user already has the pass assigned
        if (!user.passes.includes(pass._id)) {
            return res.status(409).json({ message: "The pass is not assigned to the user !" });
        }

        // Remove the pass from the user's passes list
        user.passes.pop(pass);
        // Save the user
        user.save().then(user => {
            res.status(201).json(user);
        }
        ).catch(err => {
            res.status(400).json(err);
            next(err);
        });
    } catch (err) {
        next(err)
    }
}

// Route to check if a user has access to a place
export const isUserHasAccessToPlace = async (req, res, next) => {
    try {
        // Find the user and populate the passes
        const user = await User.findOne({ user_id: req.params.user_id }).populate('passes', 'level');

        if (!user) {
            return res.status(404).json({ message: "User not found !" });
        }

        // Find the place
        const place = await Place.findOne({ place_id: req.params.place_id })


        if (!place) {
            return res.status(404).json({ message: "Place not found !" });
        }

        // Check if the user has a pass with a level equal or higher than the required_pass_level
        const isUserHasAccess = user.passes.some((pass) => pass.level >= place.required_pass_level);

        if (!isUserHasAccess) {
            return res.status(403).json({
                message: 'The user does not have access to the place',
                hasAccess: isUserHasAccess,
                additionalInfo: "The user does not have a pass with a level equal or higher than the required_pass_level",
            });
        }
        // Check if the user's age is higher than the required_age_level
        const isAgeSufficient = user.age >= place.required_age_level;

        if (!isAgeSufficient) {
            return res.status(403).json({
                message: 'The user does not have access to the place',
                hasAccess: !isUserHasAccess,
                additionalInfo: "The user's age is not higher than the required_age_level",
            });
        }
        // If the user has access and is old enough, return a success response
        if (isUserHasAccess && isAgeSufficient) {
            res.status(200).json({
                message: 'The user has access to the place',
                hasAccess: isUserHasAccess,
            });
        }
    } catch (err) {
        res.status(500).json(err);
        next(err);
    }
}

export const userAllowedPlaces = async (req, res, next) => {
    try {
        const user = await User.findOne({ user_id: req.params.user_id }).populate({ path: 'passes', model: 'Pass' });

        console.log(user);
        if (!user) {
            return res.status(404).send({ message: "User not found !" });
        }

        if (user.passes.length === 0) {
            return res.status(404).send({ message: "User has no pass !" });
        }

        // Find the places that require a pass level lower or equal to the user's highest pass level
        const passLevels = user.passes.map(pass => pass.level);

        const places = await Place.find({ required_pass_level: { $lte: Math.max(...passLevels) } });

        // Filter the places by the user's age
        const accessiblePlaces = places.filter(place => place.required_age_level <= user.age);

        res.status(200).json({ places: accessiblePlaces });
    } catch (err) {
        res.status(500).json(err);
        next(err)
    }
}
