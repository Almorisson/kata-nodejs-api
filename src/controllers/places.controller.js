import Joi from 'joi';
import Place from '../models/place.model';
import { camelToSnakeCase } from '../helpers/camel-to-snake-case.helper'

export const getAllPlaces = async (_, res, next) => {
    try {
        const places = await Place.find({});

        if (places && places.length > 0) {
            res.status(200).json(places);
        }
        else {
            res.status(404).json({ message: "No place found!" });
        }
    }
    catch (err) {
        next(err);
    }
}

export const getPlaceById = async (req, res, next) => {
    try {
        const place = await Place.findOne({ place_id: req.params.place_id })

        if (place) {
            res.status(200).json(place);
        }
        else {
            res.status(404).send({ message: "Place not found!" });
        }
    }
    catch (err) {
        next(err);
    }
}

export const createPlace = async (req, res, next) => {
    try {
        const schema = Joi.object({
            address: Joi.object({
                street: Joi.string().required(),
                city: Joi.string().required(),
                zipCode: Joi.number().integer().min(10000).max(99999).required()
            }).required(),
            phoneNumber: Joi.string().pattern(new RegExp('^[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}$')).required(),
            requiredPassLevel: Joi.number().integer().min(1).max(5).required(),
            requiredAgeLevel: Joi.number().integer().min(1).required()
        });

        const data = {
            ...req.body
        };

        const { error, value } = schema.validate(data);
        if (error) {
            // handle error
            console.error(error.details);
            return res.status(400).json({ "message": error.details[0].message })
        }

        const existingPlace = await Place.findOne({ phone_number: value.phoneNumber })

        if (existingPlace) return res.status(403).json({ "message": "Phone number is already taken!" })

        const existingPlaceAddress = await Place.findOne({
            "address.street": value.address.street,
            "address.city": value.address.city,
            "address.zip_code": value.address.zipCode,
        })

        if (existingPlaceAddress) return res.status(403).json({ "message": "Address is already taken!" })

        const place = new Place({
            ...camelToSnakeCase(value)
        });

        // save the place
        place.save().then(place => {
            res.status(201).json(place);
        }).catch(err => {
            res.status(400).json(err);
            next(err);
        });
    } catch (err) {
        next(err)
    }
}

export const updatePlace = async (req, res, next) => {
    try {
        const place = await Place.findOne({ place_id: req.params.place_id })
        if (place) {
            const updatedPlace = {
                ...place.toObject(),
                ...camelToSnakeCase(req.body),
                address: {
                    ...place.address.toObject(),
                    ...camelToSnakeCase(req.body.address)
                }
            };

            await Place.findOneAndUpdate(
                { place_id: req.params.place_id },
                { $set: updatedPlace },
                {
                    useFindAndModify: false,
                    new: true
                }
            ).then(place => {
                if (place) {
                    res.status(200).json(place);
                }
                else {
                    res.status(404).json({ message: "Place not found!" });
                }
            }).catch(err => {
                res.status(500).json(err);
                next(err);
            })
        }
        else {
            res.status(404).send({ message: "Place not found!" });
        }
    } catch (err) {
        next(err)
    }
}

export const deletePlace = async (req, res, next) => {
    try {
        await Place.findOneAndDelete({ place_id: req.params.place_id })
            .then(place => {
                if (place) {
                    // res.status(204).json(place); I prefer to return a clear message. That's why I use 200 instead of 204
                    res.status(200).json({ message: "Place deleted successfully!" });
                } else {
                    res.status(404).json({ message: "Place not found!" });
                }
            }).catch(err => {
                res.status(500).json(err);
                next(err);
            });
    } catch (err) {
        next(err)
    }
}

