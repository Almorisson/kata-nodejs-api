import jwt from 'jsonwebtoken'
import Joi from 'joi';
import User from '../models/user.model';
import { hashPassword, comparePassword } from '../utils/auth.util'
import { camelToSnakeCase } from '../helpers/camel-to-snake-case.helper'


export const register = async (req, res, next) => {
    try {
        const schema = Joi.object({
            firstName: Joi.string().min(3).max(30).required(),
            lastName: Joi.string().min(3).max(30).required(),
            email: Joi.string().email().required(),
            password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')).min(6).max(32).required(),
            age: Joi.number().integer().min(16).max(120).required(),
            phoneNumber: Joi.string().pattern(new RegExp('^[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}$')).required(),
            address: Joi.object({
                street: Joi.string().required(),
                city: Joi.string().required(),
                zipCode: Joi.number().integer().min(10000).max(99999).required()
            }).optional(),

        });

        const data = {
            ...req.body
        };

        const { error, value } = schema.validate(data);

        if (error) {
            // handle error
            console.error(error.details);
            return res.status(400).json({ "message": error.details[0].message })
        }

        const { email, password } = value;
        const existingUser = await User.findOne({ email })
        if (existingUser) return res.status(403).json({ "message": "Email is already taken!" })

        // hash the user's password
        const hashedPassword = await hashPassword(password)

        // register user
        const user = new User({
            ...camelToSnakeCase({ ...value, password: hashedPassword })
        })

        await user.save().then(newUser => {
            newUser.password = undefined;
            return res.status(201).json({ message: "Registration successful!", user: newUser });
        }).catch(err => {
            res.status(400).json(err);
            next(err);
        });
    } catch (err) {
        console.log(err);
        next(err)
    }
};


export const login = async (req, res, next) => {
    try {
        const schema = Joi.object({
            email: Joi.string().email().required(),
            password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')).min(6).max(32).required(),
        });

        const data = {
            ...req.body
        };

        const { error, value } = schema.validate(data);

        if (error) {
            // handle error
            return res.status(400).json({ "message": error.details[0].message })
        }

        const { email, password } = value;

        const existingUser = await User.findOne({ email }).select('+password -createdAt -updatedAt')
        if (!existingUser) return res.status(404).json({ "message": "No user found!" })

        // check user password
        const isPasswordMatched = await comparePassword(password, existingUser.password)
        if (!isPasswordMatched) return res.status(400).json({ "message": "Invalid credentials!" })

        // Create signIn token
        const token = jwt.sign({ user_id: existingUser.user_id }, process.env.JWT_SECRET_KEY, { expiresIn: '7h' })

        // exclude the password from user object
        existingUser.password = undefined;

        // Set the authToken in the header
        res.header('authToken', token)

        // Send user as a json response
        res.json({ user: existingUser, authToken: token, message: "Login successful!" });
    } catch (err) {
        console.log(err);
        next(err)
    }
}


export const logout = async (req, res, next) => {
    try {
        // Delete the Authorization header
        await delete req.headers['authorization'];

        return res.json({ message: "Logged out successfully!" })
    } catch (err) {
        next(err)
    }
}
