import express from "express"
import {
    getAllUsers,
    getUserById,
    createUser,
    deleteUser,
    updateUser,
} from "../../controllers/users.controller"
import { authMiddleware } from "../../middlewares/auth.middleware"

const usersRouter = express.Router()

usersRouter.get("/", [authMiddleware], getAllUsers);
usersRouter.post("/", createUser);
usersRouter.get("/:user_id", [authMiddleware], getUserById);
usersRouter.put("/:user_id", [authMiddleware], updateUser);
usersRouter.delete("/:user_id", [authMiddleware], deleteUser);

export default usersRouter;
