import express from "express"
import {
    isUserHasAccessToPlace,
    userAllowedPlaces,
} from "../../controllers/users.controller"
import { authMiddleware } from "../../middlewares/auth.middleware"

const userPlacesRouter = express.Router()

userPlacesRouter.get("/:user_id/places/:place_id", [authMiddleware], isUserHasAccessToPlace);
userPlacesRouter.get("/:user_id/places", [authMiddleware], userAllowedPlaces);

export default userPlacesRouter;
