import express from "express"
import {
    assignAPassToAUser,
    suppressAPassToAUser,
} from "../../controllers/users.controller"
import { authMiddleware } from "../../middlewares/auth.middleware"

const userPassesRouter = express.Router()

userPassesRouter.put("/:user_id/passes/:pass_id", [authMiddleware], assignAPassToAUser);
userPassesRouter.delete("/:user_id/passes/:pass_id", [authMiddleware], suppressAPassToAUser);

export default userPassesRouter;
