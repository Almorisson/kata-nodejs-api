import express from "express"
import {
    getAllPasses,
    createPass,
    getPassById,
    deletePass,
    updatePass,
} from "../../controllers/passes.controller"
import { authMiddleware } from "../../middlewares/auth.middleware"

const passesRouter = express.Router()

passesRouter.get("/", [authMiddleware], getAllPasses);
passesRouter.post("/", [authMiddleware], createPass);
passesRouter.get("/:pass_id", [authMiddleware], getPassById);
passesRouter.put("/:pass_id", [authMiddleware], updatePass);
passesRouter.delete("/:pass_id", [authMiddleware], deletePass);

export default passesRouter;
