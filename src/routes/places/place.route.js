import express from "express"
import {
    getAllPlaces,
    getPlaceById,
    createPlace,
    updatePlace,
    deletePlace,
} from "../../controllers/places.controller"
import { authMiddleware } from "../../middlewares/auth.middleware"


const placesRouter = express.Router()

placesRouter.get("/", [authMiddleware], getAllPlaces);
placesRouter.post("/", [authMiddleware], createPlace);
placesRouter.get("/:place_id", [authMiddleware], getPlaceById);
placesRouter.put("/:place_id", [authMiddleware], updatePlace);
placesRouter.delete("/:place_id", [authMiddleware], deletePlace);

export default placesRouter;
